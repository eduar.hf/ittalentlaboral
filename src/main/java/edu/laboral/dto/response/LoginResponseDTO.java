package edu.laboral.dto.response;

public class LoginResponseDTO {

	private String token;

	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	
}
