package edu.laboral.service;

import java.util.List;

import edu.laboral.dto.request.UsuarioRequestDTO;
import edu.laboral.dto.response.UsuarioResponseDTO;

public interface UsuarioService {
	
	UsuarioResponseDTO singleSelectUsuario(UsuarioRequestDTO usuarioRequest);
	List<UsuarioResponseDTO> massiveSelectUsuario(UsuarioRequestDTO usuarioRequest);
	
	UsuarioResponseDTO save(UsuarioRequestDTO usuarioRequest);
}
