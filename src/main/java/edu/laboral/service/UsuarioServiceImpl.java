package edu.laboral.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.laboral.dto.PhoneDTO;
import edu.laboral.dto.request.UsuarioRequestDTO;
import edu.laboral.dto.response.UsuarioResponseDTO;
import edu.laboral.entity.Usuario;
import edu.laboral.repository.UsuarioRepository;


@Service("usuarioService")
public class UsuarioServiceImpl implements UsuarioService{
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	public UsuarioResponseDTO singleSelectUsuario(UsuarioRequestDTO usuarioRequest) {
		
		UsuarioResponseDTO usuarioResponseDTO = new UsuarioResponseDTO();
		Usuario usuarioEntity = new Usuario();	
		List<Usuario> listUsuarioEntity = usuarioRepository.findByEmail(usuarioRequest.getEmail());		
		if(listUsuarioEntity!=null && !listUsuarioEntity.isEmpty()) {
			
			usuarioEntity = listUsuarioEntity.get(0);
			usuarioResponseDTO.setName(usuarioEntity.getName());
			usuarioResponseDTO.setEmail(usuarioEntity.getEmail());
		}
		return usuarioResponseDTO;
	}
	public List<UsuarioResponseDTO> massiveSelectUsuario(UsuarioRequestDTO usuarioRequest) {
		
		return null;
	}
	public UsuarioResponseDTO save(UsuarioRequestDTO usuarioRequest) {
		
		Usuario usuarioEntity = new Usuario();
		usuarioEntity.setUsername(usuarioRequest.getName());
		usuarioEntity.setPassword(usuarioRequest.getPassword());
		usuarioEntity.setToken(usuarioRequest.getToken());
		usuarioEntity.setName(usuarioRequest.getName());
		usuarioEntity.setIsactive("true");
		usuarioEntity.setEmail(usuarioRequest.getEmail());
		
		List<PhoneDTO> listPhones = new ArrayList<PhoneDTO>();
		for (PhoneDTO phoneDTO : usuarioRequest.getPhones()) {
			listPhones.add(phoneDTO);
		}
		
		usuarioRepository.save(usuarioEntity);
		
		UsuarioResponseDTO usuarioResponse = new UsuarioResponseDTO();
		usuarioResponse.setName(usuarioRequest.getName());
		usuarioResponse.setPassword(usuarioRequest.getPassword());
		usuarioResponse.setEmail(usuarioRequest.getEmail());
		usuarioResponse.setToken(usuarioRequest.getToken());
		usuarioResponse.setCreated(new Date());
		usuarioResponse.setModified(new Date());
		usuarioResponse.setIsactive(true);
		usuarioResponse.setPhones(listPhones);
		return usuarioResponse;
	}
	
	
}
