package edu.laboral.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import edu.laboral.dto.request.UsuarioRequestDTO;
import edu.laboral.dto.response.UsuarioResponseDTO;
import edu.laboral.service.UsuarioService;

@RestController
public class RegistroController {
	@Autowired 
	private UsuarioService usuarioService;
	
	@PostMapping("registro")
	@ResponseBody
	public ResponseEntity<UsuarioResponseDTO> registro(@Valid @RequestBody UsuarioRequestDTO usuarioRequest, HttpServletRequest req) throws Exception {
		
		
		UsuarioResponseDTO usuarioResponseDTO = usuarioService.singleSelectUsuario(usuarioRequest);
		if(usuarioResponseDTO!=null && usuarioResponseDTO.getEmail()!=null) {
			
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Correo ya existe");
		}
		
		String header = req.getHeader("Authorization");
		String authToken = header.substring(7);
		
		usuarioRequest.setToken(authToken);
		UsuarioResponseDTO usuarioResponse = usuarioService.save(usuarioRequest);
		
		
		return new ResponseEntity<>(usuarioResponse, HttpStatus.OK);
	}	
}
